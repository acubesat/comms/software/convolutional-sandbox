//
// Created by efthamar on 19/11/21.
//
#include "main.h"
#include "ConvolutionalEncoder.hpp"
#include <cstdio>
#include <iostream>

extern "C" uint32_t main_cpp(){

    ConvolutionalEncoder encoder;
    bool message[2048];

    for (int i = 0; i<2048; i++){
        message[i] = i%2;
    }

    bool output[4096];

    uint32_t before_millis = HAL_GetTick();

    encoder.encodeMessage(reinterpret_cast<bool *>(&message), reinterpret_cast<bool *>(&output));
    
    uint32_t after_millis = HAL_GetTick() - before_millis;

    return after_millis;
};

