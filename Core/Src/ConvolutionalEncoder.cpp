//
// Created by efthamar on 10/11/21.
//

#include "../Inc/ConvolutionalEncoder.hpp"
#include <iostream>

ConvolutionalEncoder::ConvolutionalEncoder() {
    inputItemsNumber = encodedCodewordLength / rate;
}

void ConvolutionalEncoder::encodeMessage(bool* inputMessageAddress, bool* outputMessageAddress) {

    for (uint8_t generatorBit = 1; generatorBit < constraintLength; generatorBit++) {
        for (uint8_t iGenerator = 0; iGenerator < rate; iGenerator++) {
            for (uint8_t stateBit = 0; stateBit < generatorBit; stateBit++) {
                *outputMessageAddress ^= inputMessageAddress[stateBit] * generator[iGenerator][constraintLength - generatorBit + stateBit];
            }
            outputMessageAddress++;
        }
    }

    for(int generatorBit = 0; generatorBit < inputItemsNumber - (constraintLength - 1); generatorBit++) {
        for (uint8_t iGenerator = 0; iGenerator < rate; iGenerator++) {
            for (uint8_t stateBit = 0; stateBit < constraintLength; stateBit++) {
                *outputMessageAddress ^= inputMessageAddress[generatorBit + stateBit] * generator[iGenerator][stateBit];
            }
            outputMessageAddress++;
        }
    }
}

ConvolutionalEncoder::~ConvolutionalEncoder() = default;