//
// Created by efthamar on 10/11/21.
//

#ifndef COMVOLUTIONAL_CONVOLUTIONALENCODER_H
#define COMVOLUTIONAL_CONVOLUTIONALENCODER_H

#include <stdint.h>

class ConvolutionalEncoder {
    /**
      * @param generator The generator polynomials used for the encoding, but inversed
      * @param rate The number of the generator polynomials
      * @param constraintLength The length of each generator polynomial
      * @param decodedCodewordLength The length of the encoded codeword
    */
protected:
    inline static const bool generator[2][7] = {{1, 0, 0, 1, 1, 1, 1}, {1, 1, 0, 1, 1, 0, 1}};
    static const uint16_t rate = 2;
    static const uint8_t constraintLength = 7;
    static const uint16_t encodedCodewordLength = 4096;
    bool encodedMessage[encodedCodewordLength]{};

public:
    uint16_t inputItemsNumber;
    /**
    * Initializes an object of the class and sets the output to be a multiple of the encodedCodewordLength.
    */
    ConvolutionalEncoder();
    /*!
    * Destroys the object of the class.
    */
    ~ConvolutionalEncoder();

    void encodeMessage(bool* inputMessageAddress, bool* outputMessageAddress);
};

#endif //COMVOLUTIONAL_CONVOLUTIONALENCODER_H
